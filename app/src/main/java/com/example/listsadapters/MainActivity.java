package com.example.listsadapters;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import android.content.Intent;
import android.content.res.Configuration;

import android.os.Bundle;
import android.os.storage.StorageManager;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;


//import com.bumptech.glide.request.Request;


import com.google.gson.Gson;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;
import okhttp3.Call;
import okhttp3.Callback;

public class MainActivity extends AppCompatActivity {

    private static StorageManager httpsReference;

    public TextView aboutView;
    RecyclerView recyclerView;

    @Nullable
    private Disposable disposable;

    public MainActivity() {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView = findViewById(R.id.recView);
            recyclerView.setAdapter(new ActorRecyclerAdapter(this, createActors()));
            recyclerView.setLayoutManager(new LinearLayoutManager(this));

            recyclerView.addOnItemTouchListener(
                    new RecyclerItemClickListener(getApplicationContext(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                        @Override public void onItemClick(View view, int position) {
                            int actorId=position;
                        //    String actorName="https://raw.github.com/square/okhttp/master/README.md";
                            Intent intent = new Intent(getApplicationContext(),ActorInfo.class);
                            intent.putExtra("ID",actorId);
                            startActivity(intent);
                        }

                        @Override public void onLongItemClick(View view, int position) {
                            Intent intent = new Intent(getApplicationContext(),ActorPortrait.class);
                            intent.putExtra("ID",position);
                            startActivity(intent);
                        }
                    })
            );
        }
        // return "Портретная ориентация";
        else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            final GridView gridview = (GridView) findViewById(R.id.gridViewActors);
            gridview.setAdapter(new ActorGridAdapter(this, createActors()));



        }
        // return "Альбомная ориентация";


    }



    public List<Actor> createActors() {


        List<Actor> actorsList = new ArrayList<>();
        actorsList.add(new Actor("Jodie Foster", "https://st.kp.yandex.net/images/actor_iphone/iphone360_26070.jpg", true,""));
        actorsList.add(new Actor("Alexandra Daddario", "https://uznayvse.ru/images/celebs/2017/5/aleksandra-daddario_big.jpg", false, ""));
        actorsList.add(new Actor("Sigourney Weaver", "https://images-na.ssl-images-amazon.com/images/I/51WsAzKbNfL._AC_.jpg", false, ""));
        actorsList.add(new Actor("Stefani Joanne Angelina Germanotta", "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Lady_Gaga_interview_2016.jpg/1200px-Lady_Gaga_interview_2016.jpg", true, ""));
        actorsList.add(new Actor("Nicole Mary Kidman", "https://i.pinimg.com/originals/04/d9/4f/04d94f9f4e778b96069472a22c7cfe54.png", true, ""));
        actorsList.add(new Actor("Milica Bogdanovna Jovovich", "https://upload.wikimedia.org/wikipedia/commons/0/0a/Milla_Jovovich_Oct_%28cropped%29.jpg", false, ""));
        actorsList.add(new Actor("Jennifer Connelly", "https://avatars.mds.yandex.net/get-kinopoisk-image/1777765/a8d36509-c96c-4e37-a28e-bdbdd61637fc/280x420", true, ""));
        actorsList.add(new Actor("Mena Suvari", "https://avatars.mds.yandex.net/get-kinopoisk-image/1777765/9b90d16b-4413-4d42-9c6f-f3fb0a66453a/280x420", false, ""));
        actorsList.add(new Actor("Vanessa Paradis", "https://avatars.mds.yandex.net/get-kinopoisk-image/1773646/d6e4062c-6fe0-48cc-9e58-1628923d687c/280x420", false, ""));
        actorsList.add(new Actor("Winona Ryder", "https://st.kp.yandex.net/im/kadr/3/4/8/kinopoisk.ru-Winona-Ryder-3485236.jpg", false, ""));
        return actorsList;
    }

    //StorageReference httpsReference = storage.getReferenceFromUrl(uri);
//document.querySelector("#__next > div > div.styles_contentContainer__171So.cz1hes0ow01e6x7djihmk__2wEpX > div.styles_mainInfoWrapper__3Dfke.styles_rootGrey__2oBAE.styles_root__3FrYl > div > div > div.styles_column__3SyAo.styles_md_16__3S4Lq.styles_lg_20__jxLMX.styles_column__V66iE > div > div > div.styles_table__3mwJ4 > div")


private void actorAbout(String myUrl){
    OkHttpClient client = new OkHttpClient();
    Request request = new Request.Builder().get().url(myUrl).build();
    Call call = client.newCall(request);

    call.enqueue(new Callback() {
        @Override
        public void onFailure(Call call, IOException e) {

        }

        @Override
        public void onResponse(Call call, Response response) throws IOException {
        String result =response.body().string();
      //  actorsAbout.add(result);
  //          Gson gson = new Gson();
//            UserDTO user = gson.fromJson(result,UserDTO.class);



        }
    });


}

    @Override
    protected void onStart() {
        super.onStart();

    }

    /*private void updateUI(Long timeWaited) {

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (disposable != null) {
            disposable.dispose();
            disposable = null;
        }
    }*/


}