package com.example.listsadapters;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ActorInfo extends AppCompatActivity {
    TextView textView;
    private static final String TAG = "myLogs";
    public String result;
   // private int actorId;
    private String myUrl;

    //will be used for longclick listener for loading gifs
    private String actorAbout(String myUrl) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().get().url(myUrl).build();
        Call call = client.newCall(request);
        // String result11="";
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // result =response.body().string();
                //  actorsAbout.add(result);
                //          Gson gson = new Gson();
//            UserDTO user = gson.fromJson(result,UserDTO.class);
                Log.d(TAG, "KOTIKKOTIKKOTIK");

                // Log.d(TAG, result);

                // Gson gson = new Gson();
                //GsonBuilder builder = new GsonBuilder();
                //Gson gson = builder.create();
                // UserDTO actor=gson.fromJson(result,UserDTO.class);

                Document doc = Jsoup.connect("http://harrix.org").get();

                Log.d(TAG, "myaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
                //  Log.d(TAG,actor.about);

            }
        });

        return result;
    }

    private String getId() {
        Intent intent = getIntent();
        int actorId =intent.getExtras().getInt("ID");
        switch (actorId) {
            case 0:
                myUrl = "https://www.imdb.com/name/nm0000149/bio?ref_=nm_ql_1";
                return myUrl;


            case 1:
                myUrl = "https://www.imdb.com/name/nm1275259/bio?ref_=nm_ql_1";
                return myUrl;


            case 2:
                myUrl = "https://www.imdb.com/name/nm0000244/bio?ref_=nm_ql_1";
                return myUrl;


            case 3:
                myUrl = "https://www.imdb.com/name/nm3078932/bio?ref_=nm_ql_1";
                return myUrl;

            //break;
            case 4:
                myUrl = "https://www.imdb.com/name/nm0000173/bio?ref_=nm_ql_1";
                return myUrl;

              //  break;
            case 5:
                myUrl = "https://www.imdb.com/name/nm0000170/bio?ref_=nm_ql_1";
                return myUrl;

                //break;
            case 6:
                myUrl = "https://www.imdb.com/name/nm0000124/bio?ref_=nm_ql_1";
                return myUrl;

                //break;
            case 7:
                myUrl = "https://www.imdb.com/name/nm0002546/bio?ref_=nm_ql_1";
                return myUrl;

                //break;
            case 8:
                myUrl = "https://www.imdb.com/name/nm0660854/bio?ref_=nm_ql_1";
                return myUrl;

               // break;
            case 9:
                myUrl = "https://www.imdb.com/name/nm0000213/bio?ref_=nm_ql_1";
                return myUrl;

               // break;

        }
        return myUrl;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actor_info);
        //intent.getStringExtra("LINK");
        getId();
        // String myUrl = "https://www.imdb.com/name/nm0000149/bio?ref_=nm_ql_1";
        String myUrl = getId();
        getWebsite(myUrl);
        textView = findViewById(R.id.textView);
        textView.setMovementMethod(new ScrollingMovementMethod());
        //  textView.setText(actorAbout(myUrl));
    }


    private void getWebsite(final String myUrl) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final StringBuilder builder = new StringBuilder();

                try {
                    Document doc = Jsoup.connect(myUrl).get();
                    String title = doc.title();
                    Elements links = doc.select("a[href]");

                    Elements content = doc.select("div#bio_content.header");
                    
                    Log.d(TAG, String.valueOf(builder.append(title).append("\n")));

                    for (Element element : content.select("p")
                    ) {
                        builder.append(element.text());
                        Log.d(TAG, String.valueOf(builder.append(element.text()).append("\n")));

                    }

                  /*  for (Element link : links) {
                        builder.append("\n").append("Link : ").append(link.attr("href"))
                                .append("\n").append("Text : ").append(link.text());
                        Log.d(TAG, builder.toString());
                    }*/
                } catch (IOException e) {
                    builder.append("Error : ").append(e.getMessage()).append("\n");
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText(builder.toString());
                    }
                });
            }
        }).start();
    }
}