package com.example.listsadapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

public class ActorGridAdapter extends BaseAdapter {

    private Context context;
    private final List<Actor> actors;

    public ActorGridAdapter(Context context, List<Actor> actors) {
        this.actors = actors;
        this.context = context;
    }

    @Override
    public int getCount() {
        return actors.size();
    }

    @Override
    public Object getItem(int position) {
        return actors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View grid;

        if (convertView == null) {
            grid = new View(context);
            //LayoutInflater inflater = getLayoutInflater();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            grid = inflater.inflate(R.layout.grid_cell_actor, parent, false);
        } else {
            grid = (View) convertView;
        }

        ImageView avatarView = (ImageView) grid.findViewById(R.id.avatarGrid);
        TextView nameView = (TextView) grid.findViewById(R.id.nameGrid);
        ImageView oscarView=(ImageView) grid.findViewById(R.id.oscarGrid);
        TextView aboutView=(TextView) grid.findViewById(R.id.aboutGrid);

        Actor actor = actors.get(position);

        nameView.setText(actor.getName());
        Glide.with(context).load(actor.getAvatar()).into(avatarView);
        oscarView.setVisibility(actor.getHasOscar()
                ? View.VISIBLE
                : View.GONE
        );
        aboutView.setText(actor.getAbout());



        grid.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,ActorInfo.class);
                intent.putExtra("ID",position);
                context.startActivity(intent);
            }
        });

        grid.setOnLongClickListener(new AdapterView.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                Intent intent = new Intent(context,ActorPortrait.class);
                intent.putExtra("ID",position);
                context.startActivity(intent);
                return true;
            }


        });

        return grid;
    }
}
