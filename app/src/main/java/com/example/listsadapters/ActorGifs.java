package com.example.listsadapters;

public class ActorGifs {

    private final String gif;

    public ActorGifs(String gif){
        this.gif=gif;
    }

    public String getGif(){return gif;}
}
