package com.example.listsadapters;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class ActorPortrait extends AppCompatActivity {

    ImageView iv_portrait;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actor_portrait);


        Intent intent = getIntent();
        int actorId =intent.getExtras().getInt("ID");

       /* iv_portrait=findViewById(R.id.iv_actorsPortrait);


        Glide.with(getApplicationContext())
                .load("https://media0.giphy.com/media/ycsoDbaI0whC33LJrw/giphy.gif")
                .into(iv_portrait);*/
        recyclerView = findViewById(R.id.recViewGifs);
        recyclerView.setAdapter(new ActorPortraitRecyclerAdapter(this, createGifs(actorId)));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private List<ActorGifs> createGifs(int actorId){
        List<ActorGifs> actorsListGifs = new ArrayList<>();
        switch (actorId) {
            case 0:
                actorsListGifs.add(new ActorGifs("https://media0.giphy.com/media/ycsoDbaI0whC33LJrw/giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media0.giphy.com/media/ycsoDbaI0whC33LJrw/giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media0.giphy.com/media/ycsoDbaI0whC33LJrw/giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media0.giphy.com/media/ycsoDbaI0whC33LJrw/giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media0.giphy.com/media/ycsoDbaI0whC33LJrw/giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media0.giphy.com/media/ycsoDbaI0whC33LJrw/giphy.gif"));
                return actorsListGifs;
            case 1:
                actorsListGifs.add(new ActorGifs("https://media0.giphy.com/media/dIlvGzTHy7lZu/giphy.gif?cid=ecf05e47d286bd586c71c2849d456efc1761eba338521d74&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media0.giphy.com/media/dIlvGzTHy7lZu/giphy.gif?cid=ecf05e47d286bd586c71c2849d456efc1761eba338521d74&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media0.giphy.com/media/dIlvGzTHy7lZu/giphy.gif?cid=ecf05e47d286bd586c71c2849d456efc1761eba338521d74&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media0.giphy.com/media/dIlvGzTHy7lZu/giphy.gif?cid=ecf05e47d286bd586c71c2849d456efc1761eba338521d74&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media0.giphy.com/media/dIlvGzTHy7lZu/giphy.gif?cid=ecf05e47d286bd586c71c2849d456efc1761eba338521d74&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media0.giphy.com/media/dIlvGzTHy7lZu/giphy.gif?cid=ecf05e47d286bd586c71c2849d456efc1761eba338521d74&rid=giphy.gif"));

                return actorsListGifs;
            case 2:
                actorsListGifs.add(new ActorGifs("https://media2.giphy.com/media/xT8qB7ENEz4Ba1fdtK/giphy.gif?cid=ecf05e47rywactp4vv0i65qdzmt3bs5b9ac7u08b2wi7obcj&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media2.giphy.com/media/xT8qB7ENEz4Ba1fdtK/giphy.gif?cid=ecf05e47rywactp4vv0i65qdzmt3bs5b9ac7u08b2wi7obcj&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media2.giphy.com/media/xT8qB7ENEz4Ba1fdtK/giphy.gif?cid=ecf05e47rywactp4vv0i65qdzmt3bs5b9ac7u08b2wi7obcj&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media2.giphy.com/media/xT8qB7ENEz4Ba1fdtK/giphy.gif?cid=ecf05e47rywactp4vv0i65qdzmt3bs5b9ac7u08b2wi7obcj&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media2.giphy.com/media/xT8qB7ENEz4Ba1fdtK/giphy.gif?cid=ecf05e47rywactp4vv0i65qdzmt3bs5b9ac7u08b2wi7obcj&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media2.giphy.com/media/xT8qB7ENEz4Ba1fdtK/giphy.gif?cid=ecf05e47rywactp4vv0i65qdzmt3bs5b9ac7u08b2wi7obcj&rid=giphy.gif"));

                return actorsListGifs;
            case 3:
                actorsListGifs.add(new ActorGifs("https://media4.giphy.com/media/3ohzdQoCngNoESFV28/giphy.gif?cid=ecf05e47lq3b57tqs1sieskmze98e4r0lnpw46mmenzodjk8&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media4.giphy.com/media/3ohzdQoCngNoESFV28/giphy.gif?cid=ecf05e47lq3b57tqs1sieskmze98e4r0lnpw46mmenzodjk8&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media4.giphy.com/media/3ohzdQoCngNoESFV28/giphy.gif?cid=ecf05e47lq3b57tqs1sieskmze98e4r0lnpw46mmenzodjk8&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media4.giphy.com/media/3ohzdQoCngNoESFV28/giphy.gif?cid=ecf05e47lq3b57tqs1sieskmze98e4r0lnpw46mmenzodjk8&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media4.giphy.com/media/3ohzdQoCngNoESFV28/giphy.gif?cid=ecf05e47lq3b57tqs1sieskmze98e4r0lnpw46mmenzodjk8&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media4.giphy.com/media/3ohzdQoCngNoESFV28/giphy.gif?cid=ecf05e47lq3b57tqs1sieskmze98e4r0lnpw46mmenzodjk8&rid=giphy.gif"));

                return actorsListGifs;
            case 4:
                actorsListGifs.add(new ActorGifs("https://media3.giphy.com/media/2poKnJu6wRSaA/giphy.gif?cid=ecf05e47pfm8h9mn2k32jpibfugybpod77ciw48e650wddc8&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media3.giphy.com/media/2poKnJu6wRSaA/giphy.gif?cid=ecf05e47pfm8h9mn2k32jpibfugybpod77ciw48e650wddc8&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media3.giphy.com/media/2poKnJu6wRSaA/giphy.gif?cid=ecf05e47pfm8h9mn2k32jpibfugybpod77ciw48e650wddc8&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media3.giphy.com/media/2poKnJu6wRSaA/giphy.gif?cid=ecf05e47pfm8h9mn2k32jpibfugybpod77ciw48e650wddc8&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media3.giphy.com/media/2poKnJu6wRSaA/giphy.gif?cid=ecf05e47pfm8h9mn2k32jpibfugybpod77ciw48e650wddc8&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media3.giphy.com/media/2poKnJu6wRSaA/giphy.gif?cid=ecf05e47pfm8h9mn2k32jpibfugybpod77ciw48e650wddc8&rid=giphy.gif"));

                return actorsListGifs;
            case 5:
                actorsListGifs.add(new ActorGifs("https://media3.giphy.com/media/lKPFZ1nPKW8c8/giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media3.giphy.com/media/lKPFZ1nPKW8c8/giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media3.giphy.com/media/lKPFZ1nPKW8c8/giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media3.giphy.com/media/lKPFZ1nPKW8c8/giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media3.giphy.com/media/lKPFZ1nPKW8c8/giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media3.giphy.com/media/lKPFZ1nPKW8c8/giphy.gif"));

                return actorsListGifs;
            case 6:
                actorsListGifs.add(new ActorGifs("https://media4.giphy.com/media/121FSRXZnDVF3q/giphy.gif?cid=ecf05e47nsntiq886spi78d835uy67z6jravdllclygq35wl&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media4.giphy.com/media/121FSRXZnDVF3q/giphy.gif?cid=ecf05e47nsntiq886spi78d835uy67z6jravdllclygq35wl&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media4.giphy.com/media/121FSRXZnDVF3q/giphy.gif?cid=ecf05e47nsntiq886spi78d835uy67z6jravdllclygq35wl&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media4.giphy.com/media/121FSRXZnDVF3q/giphy.gif?cid=ecf05e47nsntiq886spi78d835uy67z6jravdllclygq35wl&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media4.giphy.com/media/121FSRXZnDVF3q/giphy.gif?cid=ecf05e47nsntiq886spi78d835uy67z6jravdllclygq35wl&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media4.giphy.com/media/121FSRXZnDVF3q/giphy.gif?cid=ecf05e47nsntiq886spi78d835uy67z6jravdllclygq35wl&rid=giphy.gif"));

                return actorsListGifs;
            case 7:
                actorsListGifs.add(new ActorGifs("https://media1.giphy.com/media/1KtvQlgVjo1he/giphy.gif?cid=ecf05e47p1glpieyf1dl1qrir8nao9fz9vb2ypdqztkk8m1w&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media1.giphy.com/media/1KtvQlgVjo1he/giphy.gif?cid=ecf05e47p1glpieyf1dl1qrir8nao9fz9vb2ypdqztkk8m1w&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media1.giphy.com/media/1KtvQlgVjo1he/giphy.gif?cid=ecf05e47p1glpieyf1dl1qrir8nao9fz9vb2ypdqztkk8m1w&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media1.giphy.com/media/1KtvQlgVjo1he/giphy.gif?cid=ecf05e47p1glpieyf1dl1qrir8nao9fz9vb2ypdqztkk8m1w&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media1.giphy.com/media/1KtvQlgVjo1he/giphy.gif?cid=ecf05e47p1glpieyf1dl1qrir8nao9fz9vb2ypdqztkk8m1w&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media1.giphy.com/media/1KtvQlgVjo1he/giphy.gif?cid=ecf05e47p1glpieyf1dl1qrir8nao9fz9vb2ypdqztkk8m1w&rid=giphy.gif"));

                return actorsListGifs;
            case 8:
                actorsListGifs.add(new ActorGifs("https://media2.giphy.com/media/wAQhTA0Z7EcSI/giphy.gif?cid=ecf05e47d0vu5trimwk7ok0pwyn9j641wois957anahi0426&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media2.giphy.com/media/wAQhTA0Z7EcSI/giphy.gif?cid=ecf05e47d0vu5trimwk7ok0pwyn9j641wois957anahi0426&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media2.giphy.com/media/wAQhTA0Z7EcSI/giphy.gif?cid=ecf05e47d0vu5trimwk7ok0pwyn9j641wois957anahi0426&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media2.giphy.com/media/wAQhTA0Z7EcSI/giphy.gif?cid=ecf05e47d0vu5trimwk7ok0pwyn9j641wois957anahi0426&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media2.giphy.com/media/wAQhTA0Z7EcSI/giphy.gif?cid=ecf05e47d0vu5trimwk7ok0pwyn9j641wois957anahi0426&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media2.giphy.com/media/wAQhTA0Z7EcSI/giphy.gif?cid=ecf05e47d0vu5trimwk7ok0pwyn9j641wois957anahi0426&rid=giphy.gif"));

                return actorsListGifs;
            case 9:
                actorsListGifs.add(new ActorGifs("https://media1.giphy.com/media/A8LAPbKcSz9W8/giphy.gif?cid=ecf05e47i9du1ndy26vesld4qkyjazsb1jqnvn6syx63yz3v&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media1.giphy.com/media/A8LAPbKcSz9W8/giphy.gif?cid=ecf05e47i9du1ndy26vesld4qkyjazsb1jqnvn6syx63yz3v&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media1.giphy.com/media/A8LAPbKcSz9W8/giphy.gif?cid=ecf05e47i9du1ndy26vesld4qkyjazsb1jqnvn6syx63yz3v&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media1.giphy.com/media/A8LAPbKcSz9W8/giphy.gif?cid=ecf05e47i9du1ndy26vesld4qkyjazsb1jqnvn6syx63yz3v&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media1.giphy.com/media/A8LAPbKcSz9W8/giphy.gif?cid=ecf05e47i9du1ndy26vesld4qkyjazsb1jqnvn6syx63yz3v&rid=giphy.gif"));
                actorsListGifs.add(new ActorGifs("https://media1.giphy.com/media/A8LAPbKcSz9W8/giphy.gif?cid=ecf05e47i9du1ndy26vesld4qkyjazsb1jqnvn6syx63yz3v&rid=giphy.gif"));

                return actorsListGifs;
        }
        return actorsListGifs;

    }
}