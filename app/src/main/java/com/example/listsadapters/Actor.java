package com.example.listsadapters;

public class Actor {
    private final String name;
    private final String avatar;
    private final boolean hasOscar;
    private final String about;

    public Actor(String name,String avatar,boolean hasOscar,String about){
        this.name=name;
        this.avatar=avatar;
        this.hasOscar=hasOscar;
        this.about=about;
    }


    public String getName(){return name;}

    public String getAvatar(){return avatar;}

    public boolean getHasOscar(){return hasOscar;}

    public String getAbout(){return about;}


}
