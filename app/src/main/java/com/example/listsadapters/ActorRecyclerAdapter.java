package com.example.listsadapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

public class ActorRecyclerAdapter extends RecyclerView.Adapter<ActorRecyclerAdapter.ViewHolder> {

    private final List<Actor> actors;
    private final Context context;
    private final LayoutInflater inflater;

    public ActorRecyclerAdapter(Context context, List<Actor> actors) {
        this.actors = actors;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                inflater.inflate(R.layout.list_item_actor, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Actor actor = actors.get(position);

        holder.nameView.setText(actor.getName());
        Glide.with(context).load(actor.getAvatar()).into(holder.avatarView);
        holder.oscarView.setVisibility(actor.getHasOscar()
                ? View.VISIBLE
                : View.GONE
        );
       // holder.aboutView.setText(actor.getAbout());
    }

    @Override
    public int getItemCount() {
        return actors.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        public final ImageView avatarView;
        public final TextView nameView;
        public final ImageView oscarView;
        public final TextView aboutView;

        public ViewHolder(View itemView) {
            super(itemView);
            avatarView = itemView.findViewById(R.id.avatar);
            nameView = itemView.findViewById(R.id.name);
            oscarView = itemView.findViewById(R.id.oscar);
            aboutView=itemView.findViewById(R.id.about);
        }

    }



}
