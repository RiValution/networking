package com.example.listsadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class ActorPortraitRecyclerAdapter extends RecyclerView.Adapter<ActorPortraitRecyclerAdapter.ViewHolder> {
    @NonNull


    private final List<ActorGifs> actorsGifs;
    private final Context context;
    private final LayoutInflater inflater;

    public ActorPortraitRecyclerAdapter(Context context, List<ActorGifs> actorsGifs) {
        this.actorsGifs = actorsGifs;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ActorPortraitRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ActorPortraitRecyclerAdapter.ViewHolder(
                inflater.inflate(R.layout.list_item_actor_portrait, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ActorPortraitRecyclerAdapter.ViewHolder holder, int position) {
        ActorGifs actorGifs = actorsGifs.get(position);


        Glide.with(context)
                .load(actorGifs.getGif())
                .into(holder.gifView);

    }

    @Override
    public int getItemCount() {
        return actorsGifs.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        public final ImageView gifView;


        public ViewHolder(View itemView) {
            super(itemView);
            gifView = itemView.findViewById(R.id.iv_gif);

        }

    }
}


